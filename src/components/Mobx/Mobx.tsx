import React, {useEffect} from 'react';
import styles from './Mobx.module.scss';
import { observer } from 'mobx-react-lite';
import cn from 'classnames';
import Card from '../Card';
import data from '../../store/mobx/data';

const Mobx = observer(() => {
  const pages = [1,2]

  useEffect(() => {
    if(localStorage.getItem('regime')) {
      data.setRegime()
      if (localStorage.getItem('offline')) {
        // @ts-ignore
        data.setCacheData(JSON.parse(localStorage.getItem('offline')));
      }
    }
    if(!data.offline) {
      data.getProfile(data.page)
    }
  }, [data.page])

  const OfflineRegime = async () => {
    await data.setOffline(pages.length)
  }


  return (
    <div className={styles.mobx}>
      <h1>mobx</h1>
      <button onClick={OfflineRegime}>OFFLINE</button>     
      {data && data.offline && data.cacheData.length ? (
        data.cacheData[data.page - 1].data.map((el: any) => (
          <Card key={el.id} first_name={el.first_name} last_name={el.last_name} avatar={el.avatar} email= {el.email}/>
      ))
      ) : (  
        data.data.data && (
          data.data.data.map((el: any) => (
            <Card key={el.id} first_name={el.first_name} last_name={el.last_name} avatar={el.avatar} email= {el.email}/>
        ))
      ))}
      <div className={styles.mobx__pagination}>
        {pages.map((el, index) => (
              <span key={index}className={cn(styles.mobx__pagination_item, data.page === el && styles.mobx__pagination_item_active )} onClick={() => data.setPage(el)}>{el}</span> 
            ))}
      </div>
    </div>
  );
});

export default React.memo(Mobx);
