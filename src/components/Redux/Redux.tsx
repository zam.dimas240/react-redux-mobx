import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getReposApi, setPage, setOffline, setRegimeOffline, setCache} from '../../store/redux/actions';
import styles from './Redux.module.scss';
import Card from '../Card';
import cn from 'classnames';

const Redux = (() => {
    const pages = [1,2]
    const dispatch = useDispatch()

    const repos = useSelector((state: any) => state);
    React.useEffect(() => {
      if(localStorage.getItem('regime')) {
        dispatch(setRegimeOffline(true))
        if (localStorage.getItem('offline')) {
          //@ts-ignore
          dispatch(setCache(JSON.parse(localStorage.getItem('offline'))));
        }
      }
      if(!repos.data.regime) {
        dispatch(getReposApi(repos.data.page))
      }
    }, [repos.data.page])

    const OfflineRegime = () => {
      dispatch(setOffline(repos.data, 2))
    }  

    return (
      <>
        <h1 className={styles.redux}>Redux</h1>
        <button onClick={OfflineRegime}>OFFLINE</button>  
        {repos.data.cacheItems.length ? (
            repos.data.cacheItems[repos.data.page - 1].data.map((el: any) => (
              <Card key={el.id} first_name={el.first_name} last_name={el.last_name} avatar={el.avatar} email= {el.email}/>
          ))
          ) : (
        repos.data.items && (
          repos.data.items.map((el: any) => (
              <Card key={el.id} first_name={el.first_name} last_name={el.last_name} avatar={el.avatar} email= {el.email}/>
          ))
        ))}
        <div className={styles.redux__pagination}>
          {pages.map((el, index) => (
            <span 
            key={index}
            className={cn(styles.redux__pagination_item, repos.data.page === el && styles.redux__pagination_item_active )} 
            onClick={() => dispatch(setPage(el))}
            >
              {el}
            </span> 
          ))}
        </div>
      </>
    );
  }
);

export default React.memo(Redux);
