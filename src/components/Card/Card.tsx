import React from 'react';
import styles from './Card.module.scss';

export type CardProps = {
  first_name: string,
  last_name: string,
  avatar: string,
  email:string
}

const Card: React.FC<CardProps> = ({first_name, last_name, avatar, email}) => {
  return (
    <div className={styles.card}>
      <img src={avatar} alt="#" />
      <h2>{first_name} {last_name}</h2>
      <h3>{email}</h3>
    </div>
  );
};

export default React.memo(Card);
