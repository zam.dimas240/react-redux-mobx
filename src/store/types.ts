export type StoreData = {
  data: {
    page: number,
    items: [],
    cacheItems: [any],
    isLoading: boolean,
    regime: boolean,
  }
}

export type ReposType = {
  page: number,
  items: [],
  cacheItems: [any],
  isLoading: boolean,
  regime: any,
}

export type ItemsType = {
  page: string,
  first_name: string,
  last_name: string,
  avatar: string,
}