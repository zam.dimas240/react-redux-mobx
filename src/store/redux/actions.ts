import axios from "axios";
import { AppDispatch } from "./reducers";
import { SET_CACHE, SET_PAGE, SET_PROFILE, SET_REGIME } from "./types";

export const setCache = (repos: any) => ({type: SET_CACHE, payload: repos});

export const setRegimeOffline = (repos: boolean) => ({type: SET_REGIME, payload: repos});

export const setRepos = (repos: any) => ({type: SET_PROFILE, payload: repos});

export const setPage = (repos: number)=> ({type: SET_PAGE, payload: repos});

export const setOffline = (state: any, leng: number) => async (dispatch: AppDispatch) => {
  if(!state.regime) {
    dispatch(setRepos([]));
    let items:any = [];
    for (let i = 1; i <= leng; i++) {
      await axios.get(`https://reqres.in/api/users?page=${i}`)
      .then((resp) => {
        items.push(resp.data);
      })
      .catch(() => console.log('oshibka'));
    }
    dispatch(setCache(items));
    localStorage.setItem('regime', '1');
    localStorage.setItem('offline', JSON.stringify(items));
  } else {
    localStorage.setItem('regime', '');
    localStorage.setItem('offline', '');
    dispatch(setCache([]));
  }
  dispatch(setRegimeOffline(!state.regime))
}

export const getReposApi = (page: number = 1) => {
  return async (dispatch: AppDispatch) => {
    await axios.get(`https://reqres.in/api/users?page=${page}`).then((resp) => {
      const user =  resp.data.data;
      dispatch(setRepos(user))
    });
  }
}

