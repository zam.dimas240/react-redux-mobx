import {applyMiddleware, combineReducers, createStore} from "redux";
import {SET_PROFILE, SET_PAGE, SET_CACHE, SET_REGIME} from "./types";
import thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";

const defaultState = {
  items: [],
  cacheItems: [],
  regime: false,
  isLoading: true,
  page: 1,
};

function getProfile(state= defaultState, action: any) {
  switch (action.type) {
    case SET_PROFILE:
      return {...state, items: action.payload}
    case SET_PAGE: 
      return {...state, page: action.payload}
    case SET_REGIME: 
      return {...state, regime: action.payload}
    case SET_CACHE: 
      return {...state, cacheItems: action.payload}
    default:
      return state
 }
}


const rootReducer = combineReducers({
  data: getProfile,
})

export type RootState = ReturnType<typeof rootReducer>

export type AppDispatch = typeof store.dispatch

export const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));
