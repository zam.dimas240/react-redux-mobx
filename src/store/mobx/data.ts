import {makeAutoObservable} from 'mobx';
import axios from 'axios';

class Data {
  data: any = [];
  page: number =  1;
  offline: boolean = false;
  cacheData: any = [];

  constructor() {
    makeAutoObservable(this);
  }

  async getProfile(page: number = 1) {
    const apiUrl = `https://reqres.in/api/users?page=${page}`;
    await axios.get(apiUrl)
    .then((resp) => {
      const user =  resp.data;
      this.setData(user);
    })
    .catch(() => console.log('error'));
  }

  setData(data: any) {
    this.data = data;
  }

  setCacheData(data: any) {
    this.cacheData = data
  }

  setRegime() {
    this.offline = true
  }

  async setOffline(leng: number) {
    this.offline = !this.offline;
    this.setData([])
    if(this.offline) {
      for (let i = 1; i <= leng; i++) {
       await this.getProfile(i)
       await this.cacheData.push(this.data);
      }
      localStorage.setItem('regime', '1');
      localStorage.setItem('offline', JSON.stringify(this.cacheData));
    } else {
      localStorage.setItem('regime', '');
      localStorage.setItem('offline', '');
      this.cacheData = [];
    }
  }

  setPage(page: number) {
    this.page = page;
  }
}

export default new Data();