import React from 'react';
import './App.css';
import {
  Route,
  Switch,
  Redirect,
} from "react-router-dom"
import Mobx from './components/Mobx';
import Redux from './components/Redux';
import { Provider } from 'react-redux';
import { store } from './store/redux/reducers';


function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <Switch>
              <Route path='/mobx' component={Mobx} />
              <Route path='/redux' component={Redux} />
              <Redirect from='/' to='/mobx'/>
          </Switch>
      </div>
    </Provider>
  );
}

export default App;
